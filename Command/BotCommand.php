<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketBridge;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;


class BotCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tz7:discord:bot')
            ->setDescription('Run a discord bot')
            ->addOption('bot', null, InputOption::VALUE_REQUIRED, "Bot id to run")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var DiscordBotInterface $bot */
        $bot = $em->find(DiscordBotInterface::class, $input->getOption('bot'));

        /** @var DiscordWebSocketBridge $bridge */
        $bridge = $this->getContainer()->get('tz7.eve_discord.websocket_bridge');

        try {
            $bridge->run($bot);
        } catch (\Exception $ex) {
            $output->writeln(sprintf('<error>%s</error>', $ex->getMessage()));
            $output->write($ex->getTraceAsString());
        }
    }
}