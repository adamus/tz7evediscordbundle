<?php


namespace Tz7\EveDiscordBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;


class BotServiceHandlerPass implements CompilerPassInterface
{
    const BOT_SERVICES = 'tz7.eve_discord.bot_services';
    const BOT_HANDLER_TAG = 'tz7.discord_bot_handler';

    public function process(ContainerBuilder $container)
    {
        if (!$container->has(static::BOT_SERVICES)) {
            return;
        }

        $definition = $container->findDefinition(static::BOT_SERVICES);

        foreach ($container->findTaggedServiceIds(static::BOT_HANDLER_TAG) as $id => $tags) {
            $definition->addMethodCall('addServiceHandler', [$id, new Reference($id)]);
        }
    }
}
