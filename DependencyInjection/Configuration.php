<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;


class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tz7_eve_discord');

        $rootNode
            ->children()

                ->arrayNode('command_pattern')
                    ->prototype('variable')->end()
                ->end()

                ->arrayNode('price_check_systems')
                    ->prototype('variable')->end()
                ->end()

                ->arrayNode('model_map')
                    ->children()
                        ->scalarNode('service_provider')->isRequired()->end()
                        ->scalarNode('bot')->isRequired()->end()
                        ->scalarNode('bot_service')->isRequired()->end()
                        ->scalarNode('bot_channel')->isRequired()->end()
                        ->scalarNode('group')->isRequired()->end()
                        ->scalarNode('character_auth_token')->isRequired()->end()
                    ->end()
                ->end()

            ->end()
        ;

        return $treeBuilder;
    }
}
