<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;

use Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;
use Tz7\EveDiscordBundle\Model\DiscordServiceProviderInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface;
use Tz7\EveDiscordBundle\Model\DiscordGroupInterface;


class Tz7EveDiscordExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('tz7.eve_discord.model_mapping', $this->getModelMapFromConfig($config));
        $container->setParameter('tz7.eve_discord.price_check_systems', $config['price_check_systems']);

        foreach ($config['command_pattern'] as $name => $pattern) {
            $container->setParameter(sprintf('tz7.eve_discord.command_pattern.%s', $name), $pattern);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Allow an extension to prepend the extension configurations.
     *
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        // get all Bundles
        $bundles = $container->getParameter('kernel.bundles');
        if (isset($bundles['DoctrineBundle'])) {
            // Get configuration of our own bundle
            $configs = $container->getExtensionConfig($this->getAlias());
            $config = $this->processConfiguration(new Configuration(), $configs);

            // Prepare for insertion
            $forInsertion = [
                'orm' => [
                    'resolve_target_entities' => $this->getModelMapFromConfig($config)
                ]
            ];

            if ($container->hasExtension('doctrine')) {
                $container->prependExtensionConfig('doctrine', $forInsertion);
            }
        }
    }

    /**
     * @param array $config
     * @return array
     */
    protected function getModelMapFromConfig(array $config)
    {
        return [
            DiscordServiceProviderInterface::class => $config['model_map']['service_provider'],
            DiscordBotInterface::class => $config['model_map']['bot'],
            DiscordBotServiceInterface::class => $config['model_map']['bot_service'],
            DiscordBotChannelInterface::class => $config['model_map']['bot_channel'],
            DiscordGroupInterface::class => $config['model_map']['group'],
            CharacterAuthenticationTokenInterface::class => $config['model_map']['character_auth_token']
        ];
    }
}
