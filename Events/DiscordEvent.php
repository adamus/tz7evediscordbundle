<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Events;


use Symfony\Component\EventDispatcher\GenericEvent;
use Discord\Parts\Channel\Message;
use Discord\WebSockets\WebSocket;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketContext;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


final class DiscordEvent extends GenericEvent
{
    const PRE_CONNECT = 'tz7.discord.pre_connect';
    const POST_CONNECT = 'tz7.discord.post_connect';
    const ON_MESSAGE = 'tz7.discord.on_message';

    /**
     * @param DiscordWebSocketContext $context
     * @param array $arguments
     */
    public function __construct(DiscordWebSocketContext $context, array $arguments = [])
    {
        parent::__construct($context, $arguments);
    }

    /**
     * @return DiscordWebSocketContext
     */
    public function getContext()
    {
        return $this->getSubject();
    }

    /**
     * @return DiscordWebSocketServiceContext
     */
    public function getServiceContext()
    {
        if (!($this->subject instanceof DiscordWebSocketServiceContext)) {
            if (!$this->getBotService()) {
                throw new \LogicException('Can not create service context without a service.');
            }

            $this->subject = $this->subject->convertToServiceContext($this->getBotService());
        }

        return $this->subject;
    }

    /**
     * @return DiscordBotInterface
     */
    public function getBot()
    {
        return $this->getContext()->getBot();
    }

    /**
     * @return WebSocket
     */
    public function getWebSocket()
    {
        return $this->getContext()->getWebSocket();
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->getArgument('message');
    }

    /**
     * @return string
     */
    public function getMessageSenderId()
    {
        return $this->getMessage()->author->id;
    }

    /**
     * @return string
     */
    public function getMessageSenderName()
    {
        return $this->getMessage()->author->username;
    }

    /**
     * @return DiscordBotServiceInterface
     */
    public function getBotService()
    {
        return $this->getArgument('bot_service');
    }
}