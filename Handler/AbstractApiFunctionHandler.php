<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Guild\Guild;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;
use Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface;


abstract class AbstractApiFunctionHandler extends AbstractPeriodicHandler
{
    /**
     * @param DiscordWebSocketServiceContext $context
     */
    public function tick(DiscordWebSocketServiceContext $context)
    {
        /** @var Guild $server */
        $server = $context->getDiscord()->guilds->first();

        $service = $context->getService();
        $apiUsage = $service->getApiFunctionUsage();
        $apiKey = $apiUsage->getApiKey();

        $this->logger->debug(static::class, [
            'bot' => $context->getBot()->getBotName(),
            'server' => $server->name,
            'apiUsage' => (string)$apiUsage,
            'apiKey' => $apiKey ? $apiKey->getId() : null
        ]);

        $this->executeApiFunction($context, $apiUsage);
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param ApiFunctionUsageInterface $apiUsage
     */
    abstract protected function executeApiFunction(DiscordWebSocketServiceContext $context, ApiFunctionUsageInterface $apiUsage);
}
