<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Channel\Message;
use Psr\Log\LoggerInterface;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


abstract class AbstractCommandHandler extends AbstractLoggingHandler
{
    /**
     * @var string
     */
    protected $commandPattern;

    /**
     * @param LoggerInterface               $logger
     * @param BotMessageSupervisorInterface $messageSupervisor
     * @param string                        $commandPattern
     */
    public function __construct(
        LoggerInterface $logger,
        BotMessageSupervisorInterface $messageSupervisor,
        $commandPattern
    ) {
        parent::__construct($logger, $messageSupervisor);
        $this->commandPattern = $commandPattern;
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param Message                        $message
     *
     * @return bool
     */
    public function isTriggeredBy(DiscordWebSocketServiceContext $context, Message $message)
    {
        return $this->channelSelectionHelper->isEnabledOnChannel($context, $message->getChannelAttribute())
               && !!@preg_match(sprintf('/^%s$/i', $this->commandPattern), $message->content);
    }

    /**
     * @param DiscordEvent $event
     */
    public function handleMessageEvent(DiscordEvent $event)
    {
        $this->handleCommandEvent($event, $this->getCommandValues($event->getMessage()->content));
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     */
    public function tick(DiscordWebSocketServiceContext $context)
    {

    }

    /**
     * @param string $message
     *
     * @return array
     */
    protected function getCommandValues($message)
    {
        if (preg_match(sprintf('/^%s$/i', $this->commandPattern), $message, $match))
        {
            return array_slice($match, 1);
        }
        else
        {
            return [];
        }
    }

    /**
     * @param DiscordEvent $event
     * @param array        $commandValues
     */
    abstract protected function handleCommandEvent(DiscordEvent $event, array $commandValues = []);
}