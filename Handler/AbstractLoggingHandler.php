<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Channel\Channel;
use Discord\Parts\Guild\Guild;
use Psr\Log\LoggerInterface;
use Tz7\EveDiscordBundle\Handler\Helper\ChannelSelectionHelper;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;
use Tz7\EveDiscordBundle\Service\DiscordBotServiceHandlerInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


abstract class AbstractLoggingHandler implements DiscordBotServiceHandlerInterface
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var ChannelSelectionHelper */
    protected $channelSelectionHelper;

    /** @var BotMessageSupervisorInterface */
    protected $messageSupervisor;

    /**
     * @param LoggerInterface               $logger
     * @param BotMessageSupervisorInterface $messageSupervisor
     */
    public function __construct(LoggerInterface $logger, BotMessageSupervisorInterface $messageSupervisor)
    {
        $this->logger = $logger;
        $this->messageSupervisor = $messageSupervisor;
        $this->channelSelectionHelper = new ChannelSelectionHelper();
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param string                         $method
     * @param string                         $level
     * @param string                         $action
     */
    protected function logAction(DiscordWebSocketServiceContext $context, $method, $level, $action)
    {
        $this->logger->$level(sprintf('%s: %s', $method, $action));
        $this->writeToChannel(
            $context,
            sprintf('**%s**: %s', ucfirst($level), $action),
            [DiscordBotChannelInterface::TYPE_LOG]
        );
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param string                         $message
     * @param array                          $channelTypes
     */
    protected function writeToChannel(DiscordWebSocketServiceContext $context, $message, array $channelTypes = [])
    {
        if ($this->channelSelectionHelper->hasChannelTypes($context, $channelTypes))
        {
            /** @var Guild $server */
            $server = $context->getDiscord()->guilds->first();
            if (!$server instanceof Guild)
            {
                $this->logger->error(
                    sprintf(
                        '%s: Discord service #%d could not be loaded',
                        __METHOD__,
                        $context->getService()->getId()
                    )
                );

                return;
            }

            foreach ($this->channelSelectionHelper->getChannelsForTypes($context, $channelTypes) as $botChannel)
            {
                /** @var Channel $channel */
                if (($channel = $server->channels->get("id", $botChannel->getChannelId())) instanceof Channel)
                {
                    $this->messageSupervisor->sendMessageToChannel($channel, $message);
                }
                else
                {
                    $this->logAction(
                        $context,
                        __METHOD__,
                        'alert',
                        sprintf(
                            '%s: Channel #%d has been defined for botService #%d, but its not found by Discord',
                            __METHOD__,
                            $botChannel->getChannelId(),
                            $context->getService()->getId()
                        )
                    );
                }
            }

            $this->messageSupervisor->enqueueCheck($context->getWebSocket());
        }
        else
        {
            $this->logger->warning(
                sprintf(
                    '%s: No channel has been defined for botService #%d to write [%s]',
                    __METHOD__,
                    $context->getService()->getId(),
                    implode(', ', $channelTypes)
                )
            );
        }
    }
}
