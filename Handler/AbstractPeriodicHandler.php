<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Channel\Message;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


abstract class AbstractPeriodicHandler extends AbstractLoggingHandler
{
    /**
     * @param DiscordEvent $event
     */
    public function handleMessageEvent(DiscordEvent $event)
    {

    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param Message $message
     * @return bool
     */
    public function isTriggeredBy(DiscordWebSocketServiceContext $context, Message $message)
    {
        return false;
    }
}
