<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Guild\Guild;
use Discord\Parts\Guild\Role;
use Discord\Parts\User\Member;
use Discord\Helpers\Collection as DiscordCollection;
use Psr\Log\LoggerInterface;

use Tz7\EveCriterionBundle\Service\CriteriaCheckerInterface;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Manager\AbstractManager;
use Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface;
use Tz7\EveDiscordBundle\Model\DiscordGroupInterface;
use Tz7\EveDiscordBundle\Service\BotAccess;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


class CharacterAuthenticationHandler extends AbstractCommandHandler
{
    /**
     * @var AbstractManager
     */
    protected $manager;

    /**
     * @var CriteriaCheckerInterface
     */
    protected $criteriaChecker;

    /**
     * @var BotAccess
     */
    protected $botAccess;

    /**
     * @param string                        $commandPattern
     * @param AbstractManager               $manager
     * @param CriteriaCheckerInterface      $criteriaChecker
     * @param BotAccess                     $botAccess
     * @param LoggerInterface               $logger
     * @param BotMessageSupervisorInterface $messageSupervisor
     */
    public function __construct(
        $commandPattern,
        AbstractManager $manager,
        CriteriaCheckerInterface $criteriaChecker,
        BotAccess $botAccess,
        LoggerInterface $logger,
        BotMessageSupervisorInterface $messageSupervisor
    ) {
        parent::__construct($logger, $messageSupervisor, $commandPattern);

        $this->manager = $manager;
        $this->criteriaChecker = $criteriaChecker;
        $this->botAccess = $botAccess;
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     */
    public function tick(DiscordWebSocketServiceContext $context)
    {
        /** @var Guild $server */
        $server = $context->getDiscord()->guilds->first();

        $this->logger->debug(
            __METHOD__,
            [
                'bot'    => $context->getBot()->getBotName(),
                'server' => $server->name
            ]
        );

        /** @var CharacterAuthenticationTokenInterface $token */
        foreach ($this->botAccess->getBotTokens($context->getBot()) as $token)
        {
            if ($token->getDiscordClientId())
            {
                $this->updateCharacterMembership($context, $token);
            }
            $this->manager->detach($token);
        }
    }

    /**
     * @param DiscordEvent $event
     * @param array        $commandValues
     */
    protected function handleCommandEvent(DiscordEvent $event, array $commandValues = [])
    {
        $bot = $event->getBot();
        $authToken = $commandValues[0];
        $token = $this->botAccess->getBotCharacterToken($bot, $authToken);
        if ($token instanceof CharacterAuthenticationTokenInterface)
        {
            $this->logger->info(
                sprintf('%s: Found token #%d for authToken "%s"', __METHOD__, $token->getId(), $authToken)
            );

            $discordClientId = $event->getMessageSenderId();
            if (!$token->getDiscordClientId() || $token->getDiscordClientId() == $discordClientId)
            {
                $this->botAccess->acquireToken($token, $discordClientId);
                $this->updateCharacterMembership($event->getServiceContext(), $token);

                $event->getMessage()->reply('**Success**: Token acquired, role(s) added!');
                $this->logAction(
                    $event->getServiceContext(),
                    __METHOD__,
                    'info',
                    sprintf(
                        'Token #%d added to #%s',
                        $token->getId(),
                        $discordClientId
                    )
                );
            }
            else
            {
                $event->getMessage()->reply('**Failure**: Token already in use!');
                $this->logAction(
                    $event->getServiceContext(),
                    __METHOD__,
                    'alert',
                    sprintf(
                        'Token #%d already used by to #%s',
                        $token->getId(),
                        $discordClientId
                    )
                );
            }

        }
        else
        {
            $event->getMessage()->reply(sprintf('**Failure**: No token has been found for "%s"', $authToken));
            $this->logAction(
                $event->getServiceContext(),
                __METHOD__,
                'alert',
                sprintf(
                    'Token "%s" not found by message "%s"',
                    $authToken,
                    $event->getMessage()->content
                )
            );
        }
    }

    /**
     * @param DiscordWebSocketServiceContext        $context
     * @param CharacterAuthenticationTokenInterface $token
     */
    protected function updateCharacterMembership(
        DiscordWebSocketServiceContext $context,
        CharacterAuthenticationTokenInterface $token
    ) {
        /** @var Guild $server */
        $server = $context->getDiscord()->guilds->first();
        $serverRoles = $server->getRolesAttribute();

        try
        {
            $member = $server->members->get("id", $token->getDiscordClientId());
            if ($member instanceof Member)
            {
                $this->processCharacterGroups($context, $token, $member, $serverRoles);
            }
            else
            {
                throw new \Exception(sprintf('%s: Member #%d not found', __METHOD__, $token->getDiscordClientId()));
            }

        }
        catch (\Exception $ex)
        {
            $this->logAction($context, __METHOD__, 'error', $ex->getMessage());
        }
    }

    /**
     * @param DiscordWebSocketServiceContext        $context
     * @param CharacterAuthenticationTokenInterface $token
     * @param Member                                $member
     * @param DiscordCollection                     $serverRoles
     */
    protected function processCharacterGroups(
        DiscordWebSocketServiceContext $context,
        CharacterAuthenticationTokenInterface $token,
        Member $member,
        DiscordCollection $serverRoles
    ) {
        $memberRoles = $member->getRolesAttribute();

        /** @var DiscordGroupInterface $group */
        foreach ($context->getBot()->getManagedGroups() as $group)
        {
            $this->logger->info(
                sprintf(
                    '%s: Checking group "%s" for member "%s" #%d',
                    __METHOD__,
                    $group->getName(),
                    $member->username,
                    $member->id
                )
            );

            $role = $this->getGroupAssociation($serverRoles, $group);
            if ($role)
            {
                $has = !!$this->getGroupAssociation($memberRoles, $group);
                $this->checkCharacterGroup($context, $token, $group, $member, $role, $has);
            }
            else
            {
                $this->logger->alert(sprintf('%s: Role "%s" does not exists on server', __METHOD__, $group->getName()));
            }
        }
    }

    /**
     * @param DiscordWebSocketServiceContext        $context
     * @param CharacterAuthenticationTokenInterface $token
     * @param DiscordGroupInterface                 $group
     * @param Member                                $member
     * @param Role                                  $role
     * @param                                       $has
     *
     * @throws \Discord\Exceptions\PartRequestFailedException
     */
    protected function checkCharacterGroup(
        DiscordWebSocketServiceContext $context,
        CharacterAuthenticationTokenInterface $token,
        DiscordGroupInterface $group,
        Member $member,
        Role $role,
        $has
    ) {
        $registered = $token && $token->getCharacter();
        $proceed = $registered
                   && $this->criteriaChecker->testCriteria(
                $token->getCharacter(),
                $group->getCriteria(),
                $member->username
            );

        if (!$has && $registered && $proceed)
        {
            $this->logAction(
                $context,
                __METHOD__,
                'info',
                sprintf('Adding role "%s" to member "%s"', $group->getName(), $member->username)
            );

            $member->addRole($role);
            $member->save();

        }
        elseif ($has && (!$registered || !$proceed))
        {
            $this->logAction(
                $context,
                __METHOD__,
                'notice',
                sprintf('Removing role "%s" from member "%s"', $group->getName(), $member->username)
            );
            $member->removeRole($role);
            $member->save();
        }
    }

    /**
     * @param DiscordCollection     $collection
     * @param DiscordGroupInterface $group
     *
     * @return Role|null
     */
    protected function getGroupAssociation(DiscordCollection $collection, DiscordGroupInterface $group)
    {
        /** @noinspection PhpUnusedParameterInspection */
        return $collection->first(
            function ($roleId, Role $role) use ($group)
            {
                return $role->name == $group->getName();
            }
        );
    }
}
