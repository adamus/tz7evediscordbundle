<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\User\Member;
use Psr\Log\LoggerInterface;

use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\Mail\CharacterRecipientInterface;
use Tz7\EveApiBundle\Model\Mail\MailInterface;
use Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface;
use Tz7\EveApiBundle\Repository\MailRepository;
use Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;
use Tz7\EveDiscordBundle\Repository\CharacterAuthenticationTokenRepository;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


class CharacterMailHandler extends AbstractApiFunctionHandler
{
    const MAIL_PATTERN = <<<EOD
__**EVE Mail**__ *%s*
*From: %s*
*To: %s*
```Markdown
#%s
%s
```
EOD;

    /**
     * @var MailRepository
     */
    protected $mailRepository;

    /**
     * @var CharacterAuthenticationTokenRepository
     */
    protected $characterAuthenticationTokenRepository;

    /**
     * @param MailRepository                         $mailRepository
     * @param CharacterAuthenticationTokenRepository $characterAuthenticationTokenRepository
     * @param LoggerInterface                        $logger
     * @param BotMessageSupervisorInterface          $messageSupervisor
     */
    public function __construct(
        MailRepository $mailRepository,
        CharacterAuthenticationTokenRepository $characterAuthenticationTokenRepository,
        LoggerInterface $logger,
        BotMessageSupervisorInterface $messageSupervisor
    ) {
        parent::__construct($logger, $messageSupervisor);

        $this->mailRepository = $mailRepository;
        $this->characterAuthenticationTokenRepository = $characterAuthenticationTokenRepository;
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param ApiFunctionUsageInterface      $apiUsage
     */
    protected function executeApiFunction(DiscordWebSocketServiceContext $context, ApiFunctionUsageInterface $apiUsage)
    {
        $character = $apiUsage->getCharacter();
        $corporation = $character ? $character->getCorporation() : null;
        $alliance = $corporation && $corporation->getAlliance() ? $corporation->getAlliance() : null;

        /** @var CharacterAuthenticationTokenInterface[] $tokens */
        $tokens = $this->characterAuthenticationTokenRepository->findBy(['character' => $character]);

        /** @var MailInterface $mail */
        foreach ($this->mailRepository->getUnprocessedMailsForApiUsage($apiUsage) as $mail)
        {
            try
            {
                if ($this->isAllianceRecipient($mail, $alliance))
                {
                    $this->sendAllianceMail($context, $mail, $character, $alliance);
                }
                elseif ($this->isCorporationRecipient($mail, $corporation))
                {
                    $this->sendCorporationMail($context, $mail, $character, $corporation);
                }

                if ($this->isCharacterRecipient($mail, $character))
                {
                    $this->sendCharacterMail($context, $mail, $character, $tokens);
                }

            }
            catch (\Exception $ex)
            {
                $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
            }

            $this->mailRepository->markMailProcessed($mail);
        }
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param MailInterface                  $mail
     * @param CharacterInterface             $character
     * @param AllianceInterface              $alliance
     */
    protected function sendAllianceMail(
        DiscordWebSocketServiceContext $context,
        MailInterface $mail,
        CharacterInterface $character,
        AllianceInterface $alliance
    ) {
        $message = sprintf(
            static::MAIL_PATTERN,
            $mail->getSentDate()->format('Y-m-d H:i'),
            $mail->getSender()->getName(),
            sprintf('%s (%s)', $alliance->getName(), $character->getName()),
            $mail->getTitle(),
            $this->formatMailBody($mail->getBody())
        );

        $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_ALLIANCE_MEMBER]);
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param MailInterface                  $mail
     * @param CharacterInterface             $character
     * @param CorporationInterface           $corporation
     */
    protected function sendCorporationMail(
        DiscordWebSocketServiceContext $context,
        MailInterface $mail,
        CharacterInterface $character,
        CorporationInterface $corporation
    ) {
        $message = sprintf(
            static::MAIL_PATTERN,
            $mail->getSentDate()->format('Y-m-d H:i'),
            $mail->getSender()->getName(),
            sprintf('%s (%s)', $corporation->getName(), $character->getName()),
            $mail->getTitle(),
            $this->formatMailBody($mail->getBody())
        );

        $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_CORPORATION_MEMBER]);
    }

    /**
     * @param DiscordWebSocketServiceContext          $context
     * @param MailInterface                           $mail
     * @param CharacterInterface                      $character
     * @param CharacterAuthenticationTokenInterface[] $tokens
     */
    protected function sendCharacterMail(
        DiscordWebSocketServiceContext $context,
        MailInterface $mail,
        CharacterInterface $character,
        array $tokens
    ) {
        $message = sprintf(
            static::MAIL_PATTERN,
            $mail->getSentDate()->format('Y-m-d H:i'),
            $mail->getSender()->getName(),
            $character->getName(),
            $mail->getTitle(),
            $this->formatMailBody($mail->getBody())
        );

        foreach ($tokens as $token)
        {
            try
            {
                $member = $context->getDiscord()->guilds->first()->members->get("id", $token->getDiscordClientId());
                if ($member instanceof Member)
                {
                    $member->getUserAttribute()->sendMessage($message);
                }
            }
            catch (\Exception $ex)
            {
                $this->logAction($context, __METHOD__, 'error', $ex->getMessage());
            }
        }
    }

    /**
     * @param MailInterface     $mail
     * @param AllianceInterface $alliance
     *
     * @return bool
     */
    protected function isAllianceRecipient(MailInterface $mail, AllianceInterface $alliance = null)
    {
        return $mail->getToAlliance()
               && $alliance
               && $alliance->getId() == $mail->getToAlliance()->getId();
    }

    /**
     * @param MailInterface        $mail
     * @param CorporationInterface $corporation
     *
     * @return bool
     */
    protected function isCorporationRecipient(MailInterface $mail, CorporationInterface $corporation = null)
    {
        return $mail->getToCorporation()
               && $corporation
               && $corporation->getId() == $mail->getToCorporation()->getId();
    }

    /**
     * @param MailInterface      $mail
     * @param CharacterInterface $character
     *
     * @return bool
     */
    protected function isCharacterRecipient(MailInterface $mail, CharacterInterface $character)
    {
        /** @noinspection PhpUnusedParameterInspection */
        /** @noinspection PhpDocSignatureInspection */
        $filterFunction = function ($i, CharacterRecipientInterface $recipient) use ($character)
        {
            return $character->getId() == $recipient->getId();
        };

        $characterIsRecipient = $mail->getToCharacters()->exists($filterFunction);
        $characterInMailingList = $mail->getToMailingList()
            ? $mail->getToMailingList()->getMemberCharacters()->exists($filterFunction)
            : false;

        return $characterIsRecipient || $characterInMailingList;
    }

    /**
     * @param $body
     *
     * @return string
     */
    protected function formatMailBody($body)
    {
        return strip_tags(str_replace('<br>', "\n", $body));
    }
}
