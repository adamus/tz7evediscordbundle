<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\User\Member;
use Psr\Log\LoggerInterface;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\Notification\NotificationInterface;
use Tz7\EveApiBundle\Repository\NotificationRepository;
use Tz7\EveApiBundle\Service\Helper\CharacterNotificationsHelper;
use Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;
use Tz7\EveDiscordBundle\Repository\CharacterAuthenticationTokenRepository;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


class CharacterNotificationHandler extends AbstractApiFunctionHandler
{
    const NOTIFICATION_PATTERN = 'message ~ " - *" ~ from ~ ", " ~ sentDate ~ "*"';

    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    /**
     * @var CharacterAuthenticationTokenRepository
     */
    protected $characterAuthenticationTokenRepository;

    /**
     * @var CharacterNotificationsHelper
     */
    protected $helper;

    /**
     * @var ExpressionLanguage
     */
    protected $expressionLanguage;

    /**
     * @param NotificationRepository                 $notificationRepository
     * @param CharacterAuthenticationTokenRepository $characterAuthenticationTokenRepository
     * @param CharacterNotificationsHelper           $helper
     * @param LoggerInterface                        $logger
     * @param BotMessageSupervisorInterface          $messageSupervisor
     */
    public function __construct(
        NotificationRepository $notificationRepository,
        CharacterAuthenticationTokenRepository $characterAuthenticationTokenRepository,
        CharacterNotificationsHelper $helper,
        LoggerInterface $logger,
        BotMessageSupervisorInterface $messageSupervisor
    ) {
        parent::__construct($logger, $messageSupervisor);

        $this->notificationRepository = $notificationRepository;
        $this->characterAuthenticationTokenRepository = $characterAuthenticationTokenRepository;
        $this->helper = $helper;
        $this->expressionLanguage = new ExpressionLanguage();
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param ApiFunctionUsageInterface      $apiUsage
     *
     * @throws \Exception
     */
    protected function executeApiFunction(DiscordWebSocketServiceContext $context, ApiFunctionUsageInterface $apiUsage)
    {
        $character = $apiUsage->getCharacter();
        if (!$character)
        {
            $errorMessage = sprintf(
                'ApiFunctionUsage #%d-%s has no Character',
                $apiUsage->getApiKey()->getId(),
                $apiUsage->getFunction()->getName()
            );
            $this->logAction($context, __METHOD__, 'error', $errorMessage);
            throw new \Exception($errorMessage);
        }

        $corporation = $character ? $character->getCorporation() : null;
        if (!$corporation)
        {
            $errorMessage = sprintf(
                'ApiFunctionUsage #%d-%s has no Corporation from Character "%s"',
                $apiUsage->getApiKey()->getId(),
                $apiUsage->getFunction()->getName(),
                $character->getName()
            );
            $this->logAction($context, __METHOD__, 'error', $errorMessage);
            throw new \Exception($errorMessage);
        }

        $alliance = $corporation && $corporation->getAlliance() ? $corporation->getAlliance() : null;

        /** @var CharacterAuthenticationTokenInterface[] $tokens */
        $tokens = $this->characterAuthenticationTokenRepository->findBy(['character' => $character]);

        /** @var \Tz7\EveApiBundle\Model\Notification\NotificationInterface $notification */
        foreach ($this->notificationRepository->getUnprocessedNotificationsForApiUsage($apiUsage) as $notification)
        {
            $message = $this->expressionLanguage->evaluate(
                self::NOTIFICATION_PATTERN,
                [
                    'message'  => $this->helper->displayNotification($notification),
                    'from'     => $notification->getSenderName(),
                    'sentDate' => $notification->getSentDate()->format('Y-m-d H:i')
                ]
            );

            $isAllianceMemberNotification = $this->helper->isAllianceMemberNotification($notification);
            $isAllianceDirectorNotification = $this->helper->isAllianceDirectorNotification($notification);

            if ($isAllianceMemberNotification || $isAllianceDirectorNotification)
            {
                if (!$alliance)
                {
                    $errorMessage = sprintf(
                        'ApiFunctionUsage #%d-%s has no Alliance from Corporation "%s"',
                        $apiUsage->getApiKey()->getId(),
                        $apiUsage->getFunction()->getName(),
                        $corporation->getName()
                    );
                    $this->logAction($context, __METHOD__, 'error', $errorMessage);

                }
                elseif ($isAllianceMemberNotification)
                {
                    $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_ALLIANCE_MEMBER]);
                    $this->sendDebugNotification($context, $notification, $character, 'alliance_member');
                }
                else
                {
                    $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_ALLIANCE_DIRECTOR]);
                    $this->sendDebugNotification($context, $notification, $character, 'alliance_director');
                }

            }
            elseif ($this->helper->isCorporationMemberNotification($notification))
            {
                $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_CORPORATION_MEMBER]);
                $this->sendDebugNotification($context, $notification, $character, 'corporation_member');
            }
            elseif ($this->helper->isCorporationDirectorNotification($notification))
            {
                $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_CORPORATION_DIRECTOR]);
                $this->sendDebugNotification($context, $notification, $character, 'corporation_director');
            }
            elseif ($this->helper->isCharacterNotification($notification) && !empty($tokens))
            {
                $this->sendCharacterNotification($context, $message, $tokens);
                $this->sendDebugNotification($context, $notification, $character, 'character');
            }
            else
            {
                $this->sendDebugNotification($context, $notification, $character, 'unhandled');
            }

            $this->notificationRepository->markNotificationProcessed($notification);
        }
    }

    /**
     * @param DiscordWebSocketServiceContext          $context
     * @param string                                  $message
     * @param CharacterAuthenticationTokenInterface[] $tokens
     */
    protected function sendCharacterNotification(DiscordWebSocketServiceContext $context, $message, array $tokens)
    {
        foreach ($tokens as $token)
        {
            try
            {
                $member = $context->getDiscord()->guilds->first()->members->get("id", $token->getDiscordClientId());
                if ($member instanceof Member)
                {
                    $member->getUserAttribute()->sendMessage($message);
                }
            }
            catch (\Exception $ex)
            {
                $this->logAction($context, __METHOD__, 'error', $ex->getMessage());
            }
        }
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param NotificationInterface          $notification
     * @param CharacterInterface             $character
     * @param string                         $type
     */
    protected function sendDebugNotification(
        DiscordWebSocketServiceContext $context,
        NotificationInterface $notification,
        CharacterInterface $character,
        $type
    ) {
        $message = implode(
            "\n",
            [
                '**EVE Notification**',
                $notification->getSentDate()->format('Y-m-d H:i'),
                $notification->getSenderName(),
                $character->getName(),
                $type,
                $this->helper->displayNotification($notification),
                sprintf(
                    "(typeID: %d\n parameters: %s)",
                    $notification->getTypeId(),
                    json_encode($notification->getParameters())
                )
            ]
        );

        $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_ALL]);
    }
}
