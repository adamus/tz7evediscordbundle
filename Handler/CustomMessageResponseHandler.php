<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Discord;
use Discord\Parts\Channel\Message;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Handler\Helper\ChannelSelectionHelper;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;
use Tz7\EveDiscordBundle\Service\DiscordBotServiceHandlerInterface;


class CustomMessageResponseHandler implements DiscordBotServiceHandlerInterface
{
    const LIST_PATTERN     = '!response list';
    const REMEMBER_PATTERN = '\!response remember "(.*)"\s"(.*)"';
    const FORGET_PATTERN   = '\!response forget "(.*)"';

    /** @var Filesystem */
    protected $filesystem;

    /** @var string */
    protected $saveFolder;

    /** @var array */
    protected $catalogues = [];

    /** @var ChannelSelectionHelper */
    protected $channelSelectionHelper;

    /** @var BotMessageSupervisorInterface */
    protected $messageSupervisor;

    /**
     * @param string               $saveFolder
     * @param BotMessageSupervisorInterface $messageSupervisor
     */
    public function __construct($saveFolder, BotMessageSupervisorInterface $messageSupervisor)
    {
        $this->filesystem = new Filesystem();
        $this->saveFolder = $saveFolder;
        $this->channelSelectionHelper = new ChannelSelectionHelper();
        $this->messageSupervisor = $messageSupervisor;
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param Message                        $message
     *
     * @return bool
     */
    public function isTriggeredBy(DiscordWebSocketServiceContext $context, Message $message)
    {
        $this->messageSupervisor->readMessage($message);

        return $this->channelSelectionHelper->isEnabledOnChannel($context, $message->getChannelAttribute());
    }

    /**
     * @param DiscordEvent $event
     */
    public function handleMessageEvent(DiscordEvent $event)
    {
        if ($this->isSameUser($event->getContext()->getDiscord(), $event->getMessage()))
        {
            return;
        }

        $id = $event->getBot()->getId();
        $messageContent = $event->getMessage()->content;
        $response = $this->handleMessageContent($id, $messageContent);

        if (!empty($response) && !is_bool($response))
        {
            $this->messageSupervisor->sendMessageToChannel($event->getMessage()->getChannelAttribute(), $response);
            $this->messageSupervisor->enqueueCheck($event->getContext()->getWebSocket());
        }
    }

    /**
     * @param int    $catalogueId
     * @param string $pattern
     * @param string $response
     */
    public function rememberCommand($catalogueId, $pattern, $response)
    {
        $catalogue = $this->loadCatalogue($catalogueId);
        $catalogue[$pattern] = $response;
        $this->saveCatalogue($catalogueId, $catalogue);
    }

    /**
     * @param int    $catalogueId
     * @param string $pattern
     */
    public function forgetCommand($catalogueId, $pattern)
    {
        $catalogue = $this->loadCatalogue($catalogueId);
        unset($catalogue[$pattern]);
        $this->saveCatalogue($catalogueId, $catalogue);
    }

    /**
     * @param int    $catalogueId
     * @param string $messageContent
     *
     * @return bool|string
     */
    public function handleMessageContent($catalogueId, $messageContent)
    {
        if (preg_match(sprintf('/^%s$/i', static::LIST_PATTERN), $messageContent))
        {
            return sprintf('```%s```', Yaml::dump($this->loadCatalogue($catalogueId), 4));

        }
        elseif (preg_match(sprintf('/^%s$/i', static::REMEMBER_PATTERN), $messageContent, $match))
        {
            $this->rememberCommand($catalogueId, $match[1], $match[2]);

            return true;

        }
        elseif (preg_match(sprintf('/^%s$/i', static::FORGET_PATTERN), $messageContent, $match))
        {
            $this->forgetCommand($catalogueId, $match[1]);

            return true;

        }
        else
        {
            return $this->matchMessageContentToResponse($catalogueId, $messageContent);
        }
    }

    /**
     * @param int    $catalogueId
     * @param string $messageContent
     *
     * @return string|bool
     */
    public function matchMessageContentToResponse($catalogueId, $messageContent)
    {
        $catalogue = $this->loadCatalogue($catalogueId);
        foreach ($catalogue as $pattern => $response)
        {
            if (preg_match(sprintf('/^%s$/i', $pattern), $messageContent, $match))
            {
                return preg_replace("/{$pattern}/i", $response, $messageContent);
            }
        }

        return false;
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     */
    public function tick(DiscordWebSocketServiceContext $context)
    {

    }

    /**
     * @param Discord $discord
     * @param Message $message
     *
     * @return bool
     */
    protected function isSameUser(Discord $discord, Message $message)
    {
        return $discord->getClient()->username == $message->getAuthorAttribute()->username;
    }

    /**
     * @param int $catalogueId
     *
     * @return mixed
     */
    protected function loadCatalogue($catalogueId)
    {
        $file = $this->getCatalogueFileName($catalogueId);
        if (!isset($this->catalogues[$catalogueId]))
        {
            if ($this->filesystem->exists($file))
            {
                $this->catalogues[$catalogueId] = (array) Yaml::parse(@file_get_contents($file));
            }
            else
            {
                $this->catalogues[$catalogueId] = [];
            }
        }

        return $this->catalogues[$catalogueId];
    }

    /**
     * @param int   $catalogueId
     * @param array $data
     */
    protected function saveCatalogue($catalogueId, array $data)
    {
        $this->catalogues[$catalogueId] = $data;
        $this->filesystem->dumpFile($this->getCatalogueFileName($catalogueId), Yaml::dump($data, 4));
    }

    /**
     * @param int $catalogueId
     *
     * @return string
     */
    protected function getCatalogueFileName($catalogueId)
    {
        $fileName = sprintf('custom_message_response_%s.yml', $catalogueId);

        return implode(
            DIRECTORY_SEPARATOR,
            [
                $this->saveFolder,
                $fileName
            ]
        );
    }
}
