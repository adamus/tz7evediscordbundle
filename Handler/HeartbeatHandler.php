<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Channel\Message;
use Discord\Parts\Guild\Guild;
use Psr\Log\LoggerInterface;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


class HeartbeatHandler extends AbstractLoggingHandler
{
    /** @var \DateTime */
    protected $startDateTime;

    /** @var int */
    protected $messageCounter = 0;

    /** @var int */
    protected $beatCounter = 0;

    const HEARTBEAT_PATTERN = <<<EOD
__**Heartbeat**__
```Markdown
Memory usage: %s
Start: %s
Up time: %s
Messages: %d
```
EOD;

    /**
     * @inheritdoc
     */
    public function __construct(LoggerInterface $logger, BotMessageSupervisorInterface $messageSupervisor)
    {
        parent::__construct($logger, $messageSupervisor);

        $this->startDateTime = new \DateTime();

        gc_enable();
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     *
     * @throws \Exception if it beats more than it reads
     */
    public function tick(DiscordWebSocketServiceContext $context)
    {
        if ($this->beatCounter++ > $this->messageCounter)
        {
            throw new \Exception('Heart attack');
        }

        gc_collect_cycles();

        /** @var Guild $server */
        $server = $context->getDiscord()->guilds->first();
        $server->clearCache();

        $currentMemory = memory_get_usage(true);

        $message = sprintf(
            self::HEARTBEAT_PATTERN,
            $this->convertMemory($currentMemory),
            $this->startDateTime->format('Y-m-d H:i:s'),
            $this->startDateTime->diff(new \DateTime(), true)->format('%a days, %H hours, %I minutes'),
            $this->messageCounter
        );

        $this->logger->debug(sprintf('%s: "%s"', __METHOD__, $message));

        $this->writeToChannel($context, $message, [DiscordBotChannelInterface::TYPE_ALL]);
    }

    /**
     * @param DiscordEvent $event
     */
    public function handleMessageEvent(DiscordEvent $event)
    {

    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param Message                        $message
     *
     * @return bool
     */
    public function isTriggeredBy(DiscordWebSocketServiceContext $context, Message $message)
    {
        $this->messageSupervisor->readMessage($message);
        $this->messageCounter++;

        return true;
    }

    /**
     * @param int $size
     *
     * @return string
     */
    protected function convertMemory($size)
    {
        $unit = [
            'b',
            'kb',
            'mb',
            'gb',
            'tb',
            'pb'
        ];

        return @round($size / pow(1024, ($i = (int) floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }
}
