<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler\Helper;


use Discord\Parts\Channel\Channel;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;


class ChannelSelectionHelper
{
    /**
     * @param DiscordWebSocketServiceContext $context
     * @param Channel $channel
     * @return bool
     */
    public function isEnabledOnChannel(DiscordWebSocketServiceContext $context, Channel $channel)
    {
        $isPrivate = $channel->name === null;
        $botChannels = $context->getService()->getBotChannels();

        /** @noinspection PhpUnusedParameterInspection */
        return $isPrivate || $botChannels->isEmpty() || $botChannels->exists(
            function ($i, DiscordBotChannelInterface $botChannel) use ($channel) {
                return $channel->id == $botChannel->getChannelId();
            }
        );
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param array $channelTypes
     * @return bool
     */
    public function hasChannelTypes(DiscordWebSocketServiceContext $context, array $channelTypes = [])
    {
        /** @noinspection PhpUnusedParameterInspection */
        return $context->getService()->getBotChannels()->exists(
            function ($i, DiscordBotChannelInterface $botChannel) use ($channelTypes) {
                return (empty($channelTypes) || array_intersect($botChannel->getTypes(), $channelTypes))
                && $botChannel->getChannelId();
            }
        );
    }

    /**
     * @param DiscordWebSocketServiceContext $context
     * @param array $channelTypes
     * @return DiscordBotChannelInterface[]
     */
    public function getChannelsForTypes(DiscordWebSocketServiceContext $context, array $channelTypes = [])
    {
        return $context->getService()->getBotChannels()->filter(
            function (DiscordBotChannelInterface $botChannel) use ($channelTypes) {
                return (empty($channelTypes) || array_intersect($botChannel->getTypes(), $channelTypes))
                && $botChannel->getChannelId();
            }
        )->toArray();
    }
}
