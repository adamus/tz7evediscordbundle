<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Psr\Log\LoggerInterface;
use Discord\Parts\Channel\Message;
use Tz7\EveApiBundle\Exception\ApiResultException;
use Tz7\EveCentralBundle\Service\MarketStatisticsReaderInterface;
use Tz7\EveCentralBundle\Model\ItemMarketStatistics;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Service\BotMessageSupervisorInterface;


class PriceCheckHandler extends AbstractCommandHandler
{
    /** @var string[] */
    protected $systems;

    /** @var MarketStatisticsReaderInterface */
    protected $marketStatReader;

    /**
     * @param array                           $systems
     * @param MarketStatisticsReaderInterface $marketStatReader
     * @param LoggerInterface                 $logger
     * @param BotMessageSupervisorInterface   $messageSupervisor
     * @param string                          $commandPattern
     */
    public function __construct(
        array $systems,
        MarketStatisticsReaderInterface $marketStatReader,
        LoggerInterface $logger,
        BotMessageSupervisorInterface $messageSupervisor,
        $commandPattern
    ) {
        parent::__construct($logger, $messageSupervisor, $commandPattern);

        $this->systems = $systems;
        $this->marketStatReader = $marketStatReader;
    }

    /**
     * @param DiscordEvent $event
     * @param array        $commandValues
     */
    protected function handleCommandEvent(DiscordEvent $event, array $commandValues = [])
    {
        $itemName = $commandValues[0];
        $statistics = null;

        try
        {
            $event->getMessage()->getChannelAttribute()->broadcastTyping();
        }
        catch (\Exception $ex)
        {
            $this->logAction($event->getServiceContext(), __METHOD__, 'notice', $ex->getMessage());
        }

        try
        {
            $statistics = $this->marketStatReader->getItemStatisticsByName($itemName, $this->systems);
        }
        catch (ApiResultException $ex)
        {
            $message = sprintf(
                'ApiResultException "%s" calling "%s" with "%s"',
                $ex->getMessage(),
                $ex->getApiName(),
                json_encode($ex->getParameters())
            );

            while ($ex = $ex->getPrevious())
            {
                $message .= "\n" . $ex->getMessage();
            }

            $this->logAction($event->getServiceContext(), __METHOD__, 'notice', $message);
        }

        if (!empty($statistics))
        {
            $this->sendPricesReply($event->getMessage(), $itemName, $statistics);
        }
        else
        {
            $this->sendErrorReply($event->getMessage(), $itemName);
        }
    }

    /**
     * @param Message                $message
     * @param string                 $itemName
     * @param ItemMarketStatistics[] $marketStats
     */
    protected function sendPricesReply(Message $message, $itemName, array $marketStats)
    {
        $message->reply(
            "```Markdown\n" .
            sprintf('#Market statistics for "%s"', $itemName) . "\n" .
            implode(
                "\n",
                array_map(
                    function ($system, ItemMarketStatistics $stat)
                    {
                        return sprintf(
                            "* %s\n    Sell %s\n    Buy %s",
                            $system,
                            $this->formatMoney($stat->getSell()->getAvg()),
                            $this->formatMoney($stat->getBuy()->getAvg())
                        );
                    },
                    array_keys($marketStats),
                    $marketStats
                )
            ) .
            "\n```\n"
        );
    }

    /**
     * @param float $isk
     *
     * @return string
     */
    protected function formatMoney($isk)
    {
        return number_format((float) $isk, 2, '.', ' ') . ' ISK';
    }

    /**
     * @param Message $message
     * @param string  $itemName
     */
    protected function sendErrorReply(Message $message, $itemName)
    {
        $message->reply(sprintf('Prices for item "%s" not found', $itemName));
    }
}
