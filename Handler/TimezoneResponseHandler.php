<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Handler;


use Discord\Parts\Channel\Channel;
use Tz7\EveDiscordBundle\Events\DiscordEvent;


class TimezoneResponseHandler extends AbstractCommandHandler
{
    /**
     * @param DiscordEvent $event
     * @param array $commandValues
     */
    protected function handleCommandEvent(DiscordEvent $event, array $commandValues = [])
    {
        $this->sendTimeToChannel($event->getMessage()->getChannelAttribute());
    }

    /**
     * @param Channel $channel
     */
    protected function sendTimeToChannel(Channel $channel)
    {
        $timezoneMap = [
            'PST' => 'America/Los_Angeles',
            'EST' => 'America/New_York',
            'CET' => 'Europe/Copenhagen',
            'MSK' => 'Europe/Moscow',
            'AUS' => 'Australia/Sydney'
        ];

        $now = new \DateTime('now', new \DateTimeZone('UTC'));

        $timezoneList = array_map(
            function($key, $value) {
                $date = new \DateTime('now', new \DateTimeZone($value));
                return sprintf('**%s:** %s', $key, $date->format('H:i'));
            },
            array_keys($timezoneMap),
            array_values($timezoneMap)
        );

        $message = sprintf("**EVE/UTC:** %s\n%s", $now->format('Y-m-d H:i'), implode(", ", $timezoneList));

        $channel->sendMessage($message);
    }
}