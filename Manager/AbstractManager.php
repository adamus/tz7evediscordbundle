<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Manager;


use Doctrine\ORM\EntityManager;


class AbstractManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $map = [];

    /**
     * @param EntityManager $entityManager
     * @param array $map
     */
    public function __construct(EntityManager $entityManager, array $map = [])
    {
        $this->entityManager = $entityManager;
        $this->map = $map;
    }

    /**
     * @param string $model
     * @param integer $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function find($model, $id)
    {
        return $this->entityManager->find($model, $id);
    }

    /**
     * @param string $model
     * @param array $criteria
     * @return array
     */
    public function findBy($model, array $criteria = [])
    {
        return $this->entityManager->getRepository($model)->findBy($criteria);
    }

    /**
     * @param string $model
     * @param array $criteria
     * @return null|object
     */
    public function findOneBy($model, array $criteria = [])
    {
        return $this->entityManager->getRepository($model)->findOneBy($criteria);
    }

    /**
     * @param $object
     */
    public function persist($object)
    {
        $this->entityManager->persist($object);
    }

    /**
     * @param $object
     */
    public function remove($object)
    {
        $this->entityManager->remove($object);
    }

    /**
     */
    public function flush()
    {
        $this->entityManager->flush();
    }

    /**
     * @param mixed $object
     */
    public function detach($object)
    {
        $this->entityManager->detach($object);
    }

    /**
     */
    public function clear()
    {
        $this->entityManager->clear();
    }

    public function create($model)
    {
        if (isset($this->map[$model])) {
            $object = new $this->map[$model];
            return $object;
        } else {
            throw new \InvalidArgumentException(sprintf('"%s" is not mapped', $model));
        }
    }
}


