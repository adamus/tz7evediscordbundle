<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Manager;


use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;


class CharacterAuthenticationTokenManager
{
    /**
     * @var AbstractManager
     */
    protected $abstractManager;

    /**
     * @param AbstractManager $abstractManager
     */
    public function __construct(AbstractManager $abstractManager)
    {
        $this->abstractManager = $abstractManager;
    }

    /**
     * @param DiscordBotInterface $bot
     * @return CharacterAuthenticationTokenInterface[]
     */
    public function findBotTokens(DiscordBotInterface $bot)
    {
        return $this->abstractManager->findBy(CharacterAuthenticationTokenInterface::class, ['bot' => $bot]);
    }

    /**
     * @param DiscordBotInterface $bot
     * @param CharacterInterface $character
     * @return CharacterAuthenticationTokenInterface|null
     */
    public function findBotTokenByCharacter(DiscordBotInterface $bot, CharacterInterface $character)
    {
        return $this->abstractManager->findOneBy(
            CharacterAuthenticationTokenInterface::class,
            ['character' => $character, 'bot' => $bot]
        );
    }

    /**
     * @param DiscordBotInterface $bot
     * @param string $code
     * @return CharacterAuthenticationTokenInterface|null
     */
    public function findBotTokenByCode(DiscordBotInterface $bot, $code)
    {
        return $this->abstractManager->findOneBy(
            CharacterAuthenticationTokenInterface::class,
            ['authToken' => $code, 'bot' => $bot]
        );
    }

    /**
     * @param CharacterInterface $character
     * @param DiscordBotInterface $bot
     * @return CharacterAuthenticationTokenInterface
     */
    public function generateCharacterBotToken(CharacterInterface $character, DiscordBotInterface $bot)
    {
        /** @var CharacterAuthenticationTokenInterface $token */
        $token = $this->abstractManager->create(CharacterAuthenticationTokenInterface::class);

        $token->setBot($bot);
        $token->setCharacter($character);
        $token->setAuthToken(uniqid());
        $this->abstractManager->persist($token);
        $this->abstractManager->flush();

        return $token;
    }

    /**
     * @param CharacterInterface $character
     * @param DiscordBotInterface $bot
     * @return CharacterAuthenticationTokenInterface|null
     */
    public function findOrGenerateCharacterBotToken(CharacterInterface $character, DiscordBotInterface $bot)
    {
        $token = $this->findBotTokenByCharacter($bot, $character);
        if (!$token) {
            $token = $this->generateCharacterBotToken($character, $bot);
        }
        return $token;
    }

    /**
     * @param CharacterAuthenticationTokenInterface $token
     */
    public function persistToken(CharacterAuthenticationTokenInterface $token)
    {
        $this->abstractManager->persist($token);
        $this->abstractManager->flush($token);
    }

    /**
     * @param CharacterAuthenticationTokenInterface $token
     */
    public function removeToken(CharacterAuthenticationTokenInterface $token)
    {
        $this->abstractManager->remove($token);
    }
}