<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\CharacterInterface;


/**
 * @ORM\MappedSuperclass
 */
class AbstractCharacterAuthenticationToken implements CharacterAuthenticationTokenInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DiscordBotInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveDiscordBundle\Model\DiscordBotInterface", inversedBy="memberTokens")
     */
    protected $bot;

    /**
     * @var CharacterInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveApiBundle\Model\CharacterInterface")
     */
    protected $character;

    /**
     * @var integer
     * @ORM\Column(name="discord_client_id", type="bigint", nullable=true)
     */
    protected $discordClientId;

    /**
     * @var string
     * @ORM\Column(name="auth_token", type="string", length=32)
     */
    protected $authToken;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", options={"default":"2000-01-01 00:00:00"})
     */
    protected $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DiscordBotInterface
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param DiscordBotInterface $bot
     * @return $this
     */
    public function setBot(DiscordBotInterface $bot = null)
    {
        $this->bot = $bot;
        return $this;
    }

    /**
     * @return CharacterInterface
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * @param CharacterInterface $character
     * @return $this
     */
    public function setCharacter($character)
    {
        $this->character = $character;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscordClientId()
    {
        return $this->discordClientId;
    }

    /**
     * @param int $discordClientId
     * @return $this
     */
    public function setDiscordClientId($discordClientId)
    {
        $this->discordClientId = $discordClientId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @param string $authToken
     * @return $this
     */
    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}