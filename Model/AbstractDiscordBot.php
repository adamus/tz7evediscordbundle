<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\MappedSuperclass
 */
class AbstractDiscordBot implements DiscordBotInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DiscordServiceProviderInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveDiscordBundle\Model\DiscordServiceProviderInterface", inversedBy="discordBots", cascade={"persist"})
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $service;

    /**
     * @var string
     * @ORM\Column(name="server_name", type="string", length=64)
     */
    protected $serverName;

    /**
     * @var string
     * @ORM\Column(name="bot_name", type="string", length=64)
     */
    protected $botName;

    /**
     * @var string
     * @ORM\Column(name="invite_link", type="string", length=255)
     */
    protected $inviteLink;

    /**
     * @var string
     * @ORM\Column(name="token", type="string", length=255)
     */
    protected $token;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface", mappedBy="bot", cascade={"persist"})
     */
    protected $memberTokens;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Tz7\EveDiscordBundle\Model\DiscordGroupInterface", mappedBy="server", cascade={"persist"})
     */
    protected $groups;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface", mappedBy="bot", cascade={"persist"})
     */
    protected $botServices;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DiscordServiceProviderInterface
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param DiscordServiceProviderInterface $service
     * @return $this
     */
    public function setService(DiscordServiceProviderInterface $service = null)
    {
        $this->service = $service;
        return $this;
    }

    /**
     * @return string
     */
    public function getServerName()
    {
        return $this->serverName;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setServerName($name)
    {
        $this->serverName = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getBotName()
    {
        return $this->botName;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setBotName($name)
    {
        $this->botName = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getInviteLink()
    {
        return $this->inviteLink;
    }

    /**
     * @param string $inviteLink
     * @return $this
     */
    public function setInviteLink($inviteLink)
    {
        $this->inviteLink = $inviteLink;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMemberTokens()
    {
        return $this->memberTokens;
    }

    /**
     * @param CharacterAuthenticationTokenInterface $token
     * @return $this
     */
    public function addMemberToken(CharacterAuthenticationTokenInterface $token)
    {
        $token->setBot($this);
        $this->memberTokens->add($token);
        return $this;
    }

    /**
     * @param CharacterAuthenticationTokenInterface $token
     * @return $this
     */
    public function removeMemberToken(CharacterAuthenticationTokenInterface $token)
    {
        $this->memberTokens->removeElement($token);
        $token->setBot(null);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return ArrayCollection
     */
    public function getManagedGroups()
    {
        return $this->groups->filter(function(DiscordGroupInterface $group){
            return $group->isManagedGroup() && $group->getCriteria();
        });
    }

    /**
     * @param DiscordGroupInterface $group
     * @return $this
     */
    public function addGroup(DiscordGroupInterface $group)
    {
        $group->setServer($this);
        $this->groups->add($group);
        return $this;
    }

    /**
     * @param DiscordGroupInterface $group
     * @return $this
     */
    public function removeGroup(DiscordGroupInterface $group)
    {
        $this->groups->removeElement($group);
        $group->setServer(null);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBotServices()
    {
        return $this->botServices;
    }

    /**
     * @return ArrayCollection
     */
    public function getActiveBotServices()
    {
        return $this->getBotServices()->filter(function (DiscordBotServiceInterface $botService) {
            return $botService->isActive();
        });
    }

    /**
     * @param DiscordBotServiceInterface $botService
     * @return $this
     */
    public function addBotService(DiscordBotServiceInterface $botService)
    {
        $botService->setBot($this);
        $this->botServices->add($botService);
        return $this;
    }

    /**
     * @param DiscordBotServiceInterface $botService
     * @return $this
     */
    public function removeBotService(DiscordBotServiceInterface $botService)
    {
        $this->groups->removeElement($botService);
        $botService->setBot(null);
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s / %s', $this->serverName, $this->botName);
    }
}
