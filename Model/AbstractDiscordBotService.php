<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface;


/**
 * @ORM\MappedSuperclass
 */
class AbstractDiscordBotService implements DiscordBotServiceInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DiscordBotInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveDiscordBundle\Model\DiscordBotInterface", inversedBy="botServices")
     */
    protected $bot;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface", inversedBy="botServices", cascade={"persist"})
     * @ORM\JoinTable(
     *     name="tz7_discord_bot_service_has_channel",
     *     joinColumns = {
     *         @ORM\JoinColumn(
     *             name="bot_service_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     },
     *     inverseJoinColumns = {
     *         @ORM\JoinColumn(
     *             name="bot_channel_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     }
     * ))
     */
    protected $botChannels;

    /**
     * @var string
     * @ORM\Column(name="handler_type", type="string", length=64)
     */
    protected $handlerType;

    /**
     * @var integer
     * @ORM\Column(name="tick_period", type="integer", options={"default": "60"})
     * @Assert\Range(min="60", max="3600")
     */
    protected $tickPeriod;

    /**
     * @var ApiFunctionUsageInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface")
     * @ORM\JoinColumn(name="api_function_usage_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $apiFunctionUsage;

    /**
     * @var boolean
     * @ORM\Column(name="is_active", type="boolean", options={"default": "0"})
     */
    protected $active = false;

    /**
     */
    public function __construct()
    {
        $this->botChannels = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DiscordBotInterface
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param DiscordBotInterface $bot
     * @return $this
     */
    public function setBot(DiscordBotInterface $bot = null)
    {
        $this->bot = $bot;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBotChannels()
    {
        return $this->botChannels;
    }

    /**
     * @param DiscordBotChannelInterface $botChannel
     * @return $this
     */
    public function addBotChannel(DiscordBotChannelInterface $botChannel)
    {
        if (!$this->botChannels->contains($botChannel)) {
            $this->botChannels->add($botChannel);
        }
        return $this;
    }

    /**
     * @param DiscordBotChannelInterface $botChannel
     * @return $this
     */
    public function removeBotChannel(DiscordBotChannelInterface $botChannel)
    {
        $this->botChannels->add($botChannel);
        return $this;
    }

    /**
     * @return string
     */
    public function getHandlerType()
    {
        return $this->handlerType;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setHandlerType($type)
    {
        $this->handlerType = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getTickPeriod()
    {
        return $this->tickPeriod;
    }

    /**
     * @param int $tickPeriod
     * @return $this
     */
    public function setTickPeriod($tickPeriod)
    {
        $this->tickPeriod = $tickPeriod;
        return $this;
    }

    /**
     * @return \Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface
     */
    public function getApiFunctionUsage()
    {
        return $this->apiFunctionUsage;
    }

    /**
     * @param \Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface $apiFunctionUsage
     * @return $this
     */
    public function setApiFunctionUsage(ApiFunctionUsageInterface $apiFunctionUsage = null)
    {
        $this->apiFunctionUsage = $apiFunctionUsage;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s / %s', $this->getBot(), $this->handlerType);
    }
}