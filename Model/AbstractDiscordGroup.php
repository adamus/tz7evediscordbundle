<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveCriterionBundle\Model\CriteriaInterface;


/**
 * @ORM\MappedSuperclass
 */
class AbstractDiscordGroup implements DiscordGroupInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DiscordBotInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveDiscordBundle\Model\DiscordBotInterface", inversedBy="groups")
     */
    protected $server;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=64)
     */
    protected $name;

    /**
     * @var boolean
     * @ORM\Column(name="managed_group", type="boolean", options={"default": "0"})
     */
    protected $managedGroup = false;

    /**
     * @var CriteriaInterface
     * @ORM\ManyToOne(targetEntity="Tz7\EveCriterionBundle\Model\CriteriaInterface")
     * @ORM\JoinColumn(name="criteria_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $criteria;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DiscordBotInterface
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param DiscordBotInterface $server
     * @return $this
     */
    public function setServer(DiscordBotInterface $server = null)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isManagedGroup()
    {
        return $this->managedGroup;
    }

    /**
     * @param boolean $managedGroup
     * @return $this
     */
    public function setManagedGroup($managedGroup)
    {
        $this->managedGroup = $managedGroup;
        return $this;
    }

    /**
     * @return CriteriaInterface
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param CriteriaInterface $criteria
     * @return $this
     */
    public function setCriteria(CriteriaInterface $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }
}