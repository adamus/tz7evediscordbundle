<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Tz7\EveApiBundle\Model\CharacterInterface;


interface CharacterAuthenticationTokenInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return DiscordBotInterface
     */
    public function getBot();

    /**
     * @param DiscordBotInterface $bot
     * @return $this
     */
    public function setBot(DiscordBotInterface $bot = null);

    /**
     * @return CharacterInterface
     */
    public function getCharacter();

    /**
     * @param CharacterInterface $character
     * @return $this
     */
    public function setCharacter($character);

    /**
     * @return int
     */
    public function getDiscordClientId();

    /**
     * @param int $discordClientId
     * @return $this
     */
    public function setDiscordClientId($discordClientId);

    /**
     * @return string
     */
    public function getAuthToken();

    /**
     * @param string $authToken
     * @return $this
     */
    public function setAuthToken($authToken);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);
}