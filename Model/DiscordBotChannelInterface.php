<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;


interface DiscordBotChannelInterface
{
    const TYPE_ALL = 'all';
    const TYPE_LOG = 'log';
    const TYPE_PRIVATE = 'private';
    const TYPE_CORPORATION_MEMBER = 'corporation_member';
    const TYPE_CORPORATION_DIRECTOR = 'corporation_director';
    const TYPE_ALLIANCE_MEMBER = 'alliance_member';
    const TYPE_ALLIANCE_DIRECTOR = 'alliance_director';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return ArrayCollection
     */
    public function getBotServices();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getChannelId();

    /**
     * @param string $channelId
     * @return $this
     */
    public function setChannelId($channelId);

    /**
     * @return array
     */
    public function getTypes();

    /**
     * @param array $types
     * @return $this
     */
    public function setTypes(array $types);
}