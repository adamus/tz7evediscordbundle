<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;


interface DiscordBotInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return DiscordServiceProviderInterface
     */
    public function getService();

    /**
     * @param DiscordServiceProviderInterface $service
     * @return $this
     */
    public function setService(DiscordServiceProviderInterface $service = null);

    /**
     * @return string
     */
    public function getServerName();

    /**
     * @param string $name
     * @return $this
     */
    public function setServerName($name);

    /**
     * @return string
     */
    public function getBotName();

    /**
     * @param string $name
     * @return $this
     */
    public function setBotName($name);

    /**
     * @return string
     */
    public function getInviteLink();
    /**
     * @param string $inviteLink
     * @return $this
     */
    public function setInviteLink($inviteLink);

    /**
     * @return string
     */
    public function getToken();

    /**
     * @param string $token
     * @return $this
     */
    public function setToken($token);

    /**
     * @return ArrayCollection
     */
    public function getMemberTokens();

    /**
     * @param CharacterAuthenticationTokenInterface $token
     * @return $this
     */
    public function addMemberToken(CharacterAuthenticationTokenInterface $token);

    /**
     * @param CharacterAuthenticationTokenInterface $token
     * @return $this
     */
    public function removeMemberToken(CharacterAuthenticationTokenInterface $token);

    /**
     * @return ArrayCollection
     */
    public function getGroups();

    /**
     * @return ArrayCollection
     */
    public function getManagedGroups();

    /**
     * @param DiscordGroupInterface $group
     * @return $this
     */
    public function addGroup(DiscordGroupInterface $group);

    /**
     * @param DiscordGroupInterface $group
     * @return $this
     */
    public function removeGroup(DiscordGroupInterface $group);

    /**
     * @return ArrayCollection
     */
    public function getBotServices();

    /**
     * @return ArrayCollection
     */
    public function getActiveBotServices();

    /**
     * @param DiscordBotServiceInterface $botService
     * @return $this
     */
    public function addBotService(DiscordBotServiceInterface $botService);

    /**
     * @param DiscordBotServiceInterface $botService
     * @return $this
     */
    public function removeBotService(DiscordBotServiceInterface $botService);
}