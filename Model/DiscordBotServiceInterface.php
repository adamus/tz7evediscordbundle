<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;
use Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface;


interface DiscordBotServiceInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return DiscordBotInterface
     */
    public function getBot();

    /**
     * @param DiscordBotInterface $bot
     * @return $this
     */
    public function setBot(DiscordBotInterface $bot = null);

    /**
     * @return ArrayCollection
     */
    public function getBotChannels();

    /**
     * @param DiscordBotChannelInterface $botChannel
     * @return $this
     */
    public function addBotChannel(DiscordBotChannelInterface $botChannel);

    /**
     * @param DiscordBotChannelInterface $botChannel
     * @return $this
     */
    public function removeBotChannel(DiscordBotChannelInterface $botChannel);

    /**
     * @return string
     */
    public function getHandlerType();

    /**
     * @param string $type
     * @return $this
     */
    public function setHandlerType($type);

    /**
     * @return integer
     */
    public function getTickPeriod();

    /**
     * @param integer $period
     * @return $this
     */
    public function setTickPeriod($period);

    /**
     * @return \Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface
     */
    public function getApiFunctionUsage();

    /**
     * @param \Tz7\EveApiBundle\Model\Api\ApiFunctionUsageInterface $apiFunctionUsage
     * @return $this
     */
    public function setApiFunctionUsage(ApiFunctionUsageInterface $apiFunctionUsage = null);

    /**
     * @return boolean
     */
    public function isActive();

    /**
     * @param boolean $active
     * @return $this
     */
    public function setActive($active);
}