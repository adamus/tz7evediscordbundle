<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Model;


use Tz7\EveCriterionBundle\Model\CriteriaInterface;


interface DiscordGroupInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return DiscordBotInterface
     */
    public function getServer();

    /**
     * @param DiscordBotInterface $server
     * @return $this
     */
    public function setServer(DiscordBotInterface $server = null);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return boolean
     */
    public function isManagedGroup();

    /**
     * @param boolean $managedGroup
     * @return $this
     */
    public function setManagedGroup($managedGroup);

    /**
     * @return CriteriaInterface
     */
    public function getCriteria();

    /**
     * @param CriteriaInterface $criteria
     * @return $this
     */
    public function setCriteria(CriteriaInterface $criteria = null);
}