TZ-7: EveDiscordBundle
======

This Symfony 2 bundle is part of the [TZ-7 project](https://bitbucket.org/adamus/tz-7), 
providing EVE Online auth and tool bot(s) for Discord.

* [Concepts and basics](Resources/doc/index.md)
* [How to create a Custom Service Handler](Resources/doc/custom_service_handler.md)


## Installation ##

### Requirements ###

* [Tz7\EveApiBundle](https://bitbucket.org/adamus/tz7eveapibundle) for accessing EVE API data (eg.: Notifications)
* [Tz7\EveCriterionBundle](https://bitbucket.org/adamus/tz7evecriterionbundle) for access management
* [Tz7\EveCentralBundle](https://bitbucket.org/adamus/tz7evecentralbundle) for EVE Central API data (eg.: Market Statistics)

### Composer ###

Extend the repositories in composer.json:
    
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:adamus/tz7eveapibundle.git"
        },
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:adamus/tz7evecriterionbundle.git"
        },
        {
            "type": "vcs",
            "url":  "git@github.com:adamus-tork/DiscordPHP.git"
        }
    ],

  
Then run the following command:

    composer require tz7/eve-discord-bundle:dev-master


## Integration ##

Bundle needs to be registered in the AppKernel:

    new Tz7\EveDiscordBundle\Tz7EveDiscordBundle(),


Implement the following interfaces:

* [CharacterAuthenticationTokenInterface](Model/CharacterAuthenticationTokenInterface.php)
* [DiscordBotInterface](Model/DiscordBotInterface.php)
* [DiscordBotServiceInterface](Model/DiscordBotServiceInterface.php)
* [DiscordGroupInterface](Model/DiscordGroupInterface.php)


The implemented entities needs to be defined in config for the correct model:
    
    tz7_eve_discord:
        command_pattern:
            character_authentication: "\!auth ([^$]+)"
            time_zone: "\!time"
            price_check: "\!pc ([^$]+)"
        price_check_systems: [Amarr, Jita, Rens, Dodixie]
        model_map:
            service_provider: YourBundle\Entity\YourService
            bot: YourBundle\Entity\YourDiscordBot
            bot_service: YourBundle\Entity\YourDiscordBotService
            bot_channel: YourBundle\Entity\YourDiscordBotChannel
            group: YourBundle\Entity\YourDiscordGroup
            character_auth_token: YourBundle\Entity\YourDiscordCharacterAuthToken
