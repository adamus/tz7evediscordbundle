<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveCriterionBundle\Service\CriteriaCheckerInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Model\DiscordGroupInterface;



class DiscordBotRepository extends EntityRepository
{
    /**
     * @var CriteriaCheckerInterface
     */
    protected $criteriaChecker;

    /**
     * @param CriteriaCheckerInterface $criteriaChecker
     */
    public function setCriteriaChecker(CriteriaCheckerInterface $criteriaChecker)
    {
        $this->criteriaChecker = $criteriaChecker;
    }

    /**
     * @param CharacterInterface $character
     * @return DiscordBotInterface[]
     */
    public function getAvailableBotsForCharacter(CharacterInterface $character)
    {
        /** @var DiscordBotInterface[] $bots */
        $bots = $this->createQueryBuilder('bot')
            ->join('bot.groups', 'grp')->addSelect('grp')
            ->join('grp.criteria', 'cia')->addSelect('cia')
            ->join('cia.criteria', 'con')->addSelect('con')
            ->where('grp.managedGroup = true')
            ->getQuery()->getResult()
        ;

        /** @var DiscordBotInterface[] $filtered */
        $filtered = array_filter(
            $bots,
            function ($bot) use ($character) {
                return $this->checkBotCharacterAccess($bot, $character);
            }
        );

        return $filtered;
    }

    /**
     * @param DiscordBotInterface $bot
     * @param CharacterInterface $character
     * @return bool
     */
    protected function checkBotCharacterAccess(DiscordBotInterface $bot, CharacterInterface $character)
    {
        /** @var DiscordGroupInterface $group */
        foreach ($bot->getManagedGroups() as $group) {
            if ($this->criteriaChecker->testCriteria($character, $group->getCriteria(), $character->getName())) {
                return true;
            }
        }
        return false;
    }
}
