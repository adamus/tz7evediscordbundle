TZ-7: EveDiscordBundle
======

## Custom Service Handler ##

Lets say you want to add a service to the Discord Bot, so it could repeatedly display the current time to a channel. 
To achieve this, you need to:

1. Implement the handler
2. Register the service in Symfony
3. Attaching bot(s) and channel(s) 


### Implementation ###

You could create a new class, implementing [DiscordBotServiceHandlerInterface](../../Service/DiscordBotServiceHandlerInterface.php),
but for periodic services there is [AbstractPeriodicHandler](../../Handler/AbstractPeriodicHandler.php) prepared.

```php
<?php

namespace Your\Handlers;

use Tz7\EveDiscordBundle\Handler\AbstractPeriodicHandler;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketServiceContext;

class YourPeriodicTimeHandler extends AbstractPeriodicHandler
{
    /**
     * @param DiscordWebSocketServiceContext $context
     */
    public function tick(DiscordWebSocketServiceContext $context)
    {
        $this->writeToChannel($context, date('Y-m-d H:i:s'));
    }
}
```

That's it. Method tick() is will be executed periodically and writeToChannel() will look for enabled channels of the 
 service (configured in step 3) and sends the message.


### Service config ###

    # services.yml
    your.handler.periodic_time:
        class: Your\Handlers\YourPeriodicTimeHandler
        arguments:
            - '@logger'
        tags:
            - {name: tz7.discord_bot_handler}
            
Tag __"tz7.discord_bot_handler" is required__ so [BotServices](../../Service/BotServices.php) can load it.


### Attaching bot(s) and channel(s) ###

The last step is setting up the relations between Bot, Service Handler and Channel, as it is runtime config and can
 be changed from the database, preferably from some admin CRUD, like Sonata Admin.
 
```php
<?php

$bot = new DiscordBot(); // bot configured to connect

$botService = new DiscordBotService(); // service provided by the bot (handler)
$botService->setTickPeriod(3600); // Call tick() every hour.
$botService->setHandlerType('your.handler.periodic_time'); // Service id
$botService->setActive(true); // Services can be deactivated

$botChannel = new DiscordBotChannel(); // channel configuration (id, scope) ~ where can the bot write
$botChannel->setChannelId(/* To obtain channel ID from Discord, see Discord documentation */);
$botChannel->setName(/* Name the channel so you won't need to refer to it's id within this app */);
$botChannel->setTypes([DiscordBotChannelInterface::TYPE_CORPORATION_MEMBER]); // Scope of the messages

// Associations
$botService->addBotChannel($botChannel);
$bot->addBotService($botService);
```

This configuration will be used by [DiscordWebSocketBridge](../../WebSocket/DiscordWebSocketBridge.php), which will 
trigger or tick only active services which also receives the configuration (Context) so they should send messages to the
 selected channel(s).

To easily obtain Server ID and Channel ID you should [set your Discord App to developer mode](https://support.discordapp.com/hc/en-us/articles/206346498-Where-can-I-find-my-server-ID-).