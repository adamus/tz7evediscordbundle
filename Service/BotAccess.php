<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Service;


use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveDiscordBundle\Model\CharacterAuthenticationTokenInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Repository\DiscordBotRepository;
use Tz7\EveDiscordBundle\Manager\CharacterAuthenticationTokenManager;


class BotAccess
{
    /**
     * @var DiscordBotRepository
     */
    protected $botRepository;

    /**
     * @var CharacterAuthenticationTokenManager
     */
    protected $tokenManager;

    /**
     * @param DiscordBotRepository $botRepository
     * @param CharacterAuthenticationTokenManager $tokenManager
     */
    public function __construct(DiscordBotRepository $botRepository, CharacterAuthenticationTokenManager $tokenManager)
    {
        $this->botRepository = $botRepository;
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param CharacterInterface $character
     * @return array
     */
    public function getBotAccessOfCharacter(CharacterInterface $character)
    {
        return array_map(
            function (DiscordBotInterface $bot) use ($character) {
                return [
                    'bot' => $bot,
                    'token' => $this->tokenManager->findOrGenerateCharacterBotToken($character, $bot)
                ];
            },
            $this->botRepository->getAvailableBotsForCharacter($character)
        );
    }

    /**
     * @param CharacterAuthenticationTokenInterface $token
     * @param string $discordClientId
     */
    public function acquireToken(CharacterAuthenticationTokenInterface $token, $discordClientId)
    {
        $token->setDiscordClientId($discordClientId);
        $this->tokenManager->persistToken($token);
    }

    /**
     * @param DiscordBotInterface $bot
     * @return CharacterAuthenticationTokenInterface[]
     */
    public function getBotTokens(DiscordBotInterface $bot)
    {
        return $this->tokenManager->findBotTokens($bot);
    }

    /**
     * @param DiscordBotInterface $bot
     * @param $authToken
     * @return CharacterAuthenticationTokenInterface|null
     */
    public function getBotCharacterToken(DiscordBotInterface $bot, $authToken)
    {
        return $this->tokenManager->findBotTokenByCode($bot, $authToken);
    }
}