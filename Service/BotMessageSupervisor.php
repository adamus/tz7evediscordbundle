<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Service;


use Discord\Parts\Channel\Channel;
use Discord\Parts\Channel\Message;
use Discord\WebSockets\WebSocket;
use Psr\Log\LoggerInterface;


class BotMessageSupervisor implements BotMessageSupervisorInterface
{
    const QUEUE_CHECK = 30;

    /** @var array */
    private $queue = [];

    /** @var LoggerInterface */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Channel $channel
     * @param string  $messageContent
     */
    public function sendMessageToChannel(Channel $channel, $messageContent)
    {
        $channel->broadcastTyping();
        foreach (str_split($messageContent, 1800) as $split)
        {
            $channel->sendMessage($split);
            sleep(1);
        }

        if (!isset($this->queue[$channel->id]))
        {
            $this->queue[$channel->id] = [];
        }

        $this->logger->debug(
            sprintf(
                '%s: Sent message "%s" to channel #%s',
                __METHOD__,
                $messageContent,
                $channel->id
            ),
            $this->queue
        );

        $this->queue[$channel->id][time()] = md5($messageContent);
    }

    /**
     * @param Message $message
     */
    public function readMessage(Message $message)
    {
        $channel = $message->getChannelAttribute();
        $messageHash = md5($message->content);

        if (!isset($this->queue[$channel->id]))
        {
            $this->queue[$channel->id] = [];

            return;
        }

        $index = array_search($messageHash, $this->queue[$channel->id]);

        if ($index !== false)
        {
            unset($this->queue[$channel->id][$index]);

            $this->logger->debug(
                sprintf(
                    '%s: Read message "%s" from channel #%s',
                    __METHOD__,
                    $messageHash,
                    $channel->id
                ),
                $this->queue
            );
        }
        else
        {
            $this->logger->debug(
                sprintf(
                    '%s: Read message "%s" not queued',
                    __METHOD__,
                    $messageHash
                ),
                $this->queue
            );
        }
    }

    /**
     * @param WebSocket $webSocket
     */
    public function enqueueCheck(WebSocket $webSocket)
    {
        $this->logger->debug(__METHOD__);

        $webSocket->loop->addTimer(
            self::QUEUE_CHECK,
            function ()
            {
                $this->checkQueue();
            }
        );
    }

    /**
     * @throws \Exception
     */
    public function checkQueue()
    {
        $this->logger->debug(__METHOD__, $this->queue);
        $limit = self::QUEUE_CHECK / 2;

        foreach ($this->queue as $channelId => $channelQueue)
        {
            foreach (array_keys($channelQueue) as $time)
            {
                if ((time() - $time) > $limit)
                {
                    throw new \Exception(
                        sprintf(
                            '%s: Did not received outgoing message "%s" on channel "%s',
                            __METHOD__,
                            $channelQueue[$time],
                            $channelId
                        )
                    );
                }
            }
        }
    }
}
