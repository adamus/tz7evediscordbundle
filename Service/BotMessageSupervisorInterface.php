<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Service;


use Discord\Parts\Channel\Channel;
use Discord\Parts\Channel\Message;
use Discord\WebSockets\WebSocket;


interface BotMessageSupervisorInterface
{
    /**
     * @param Channel $channel
     * @param string  $messageContent
     */
    public function sendMessageToChannel(Channel $channel, $messageContent);

    /**
     * @param Message $message
     */
    public function readMessage(Message $message);

    /**
     * @param WebSocket $webSocket
     */
    public function enqueueCheck(WebSocket $webSocket);

    /**
     * @throws \Exception
     */
    public function checkQueue();
}
