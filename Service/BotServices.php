<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Service;


use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface;
use Tz7\EveDiscordBundle\WebSocket\DiscordWebSocketContext;


class BotServices
{
    /**
     * @var DiscordBotServiceHandlerInterface[]
     */
    protected $serviceHandlers = [];

    /**
     * @param string
     * @param DiscordBotServiceHandlerInterface $serviceHandler
     * @return $this
     */
    public function addServiceHandler($id, DiscordBotServiceHandlerInterface $serviceHandler)
    {
        $this->serviceHandlers[$id] = $serviceHandler;
        return $this;
    }

    /**
     * @return array
     */
    public function getServiceHandlerTypes()
    {
        return array_keys($this->serviceHandlers);
    }

    /**
     * @param $id
     * @return bool
     */
    public function hasServiceHandler($id)
    {
        return isset($this->serviceHandlers[$id]);
    }

    /**
     * @param string $id
     * @return DiscordBotServiceHandlerInterface
     */
    public function getServiceHandler($id)
    {
        return $this->serviceHandlers[$id];
    }

    /**
     * @param string $id
     * @param DiscordEvent $event
     */
    public function handleMessageEvent($id, DiscordEvent $event)
    {
        if ($this->hasServiceHandler($id)) {
            $this->getServiceHandler($id)->handleMessageEvent($event);
        }
    }

    /**
     * @param DiscordWebSocketContext $context
     */
    public function tick(DiscordWebSocketContext $context)
    {
        /** @var DiscordBotServiceInterface $botService */
        foreach ($context->getBot()->getActiveBotServices() as $botService) {
            if ($this->hasServiceHandler($botService->getHandlerType())) {
                $this->getServiceHandler($botService->getHandlerType())->tick(
                    $context->convertToServiceContext($botService)
                );
            }
        }
    }
}