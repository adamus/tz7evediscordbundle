<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Tests\Service;

use Tz7\EveDiscordBundle\Handler\CustomMessageResponseHandler;
use Tz7\EveApiBundle\Tests\KernelTestCase;


class CustomMessageHandlerTest extends KernelTestCase
{
    public function testRegExp()
    {
        $handler = self::getCustomMessageResponseHandler();
        $catalogueId = 0;
        $tests = [
            [
                "Egyél (szőlőt|almát)", // pattern
                "almaaa", // response
                [
                    "Egyél almát" => "almaaa", // test message => excepted response
                    "Egyél banánt" => false
                ]
            ],
            [
                "Egyél (csokit|kekszet)",
                "almát vagy $1?",
                [
                    "Egyél csokit" => "almát vagy csokit?",
                    "Egyél banánt" => false
                ]
            ],
            [
                '@([\w|\s]+):.*',
                "$1!!!",
                [
                    "@Adamus TorK: vagy?" => "Adamus TorK!!!"
                ]
            ]
        ];

        foreach ($tests as $case) {
            $messageContent = sprintf('!response remember "%s" "%s"', $case[0], $case[1]);
            $this->assertTrue($handler->handleMessageContent($catalogueId, $messageContent));

            foreach ($case[2] as $message => $expected) {
                $response = $handler->matchMessageContentToResponse($catalogueId, $message);
                $this->assertEquals($expected, $response);
            }

            $messageContent = sprintf('!response forget "%s"', $case[0]);
            $this->assertTrue($handler->handleMessageContent($catalogueId, $messageContent));
        }
    }

    /**
     * @return CustomMessageResponseHandler
     */
    protected static function getCustomMessageResponseHandler()
    {
        return static::getContainer()->get('tz7.eve_discord.handler.custom_message_response');
    }
}