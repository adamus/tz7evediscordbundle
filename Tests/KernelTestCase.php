<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\Tests;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as BaseTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;


class KernelTestCase extends BaseTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    protected static function getContainer()
    {
        if (!static::$container) {
            static::$container = static::getKernel()->getContainer();
        }
        return static::$container;
    }

    protected static function getKernel()
    {
        if (!static::$kernel) {
            static::bootKernel();
        }
        return static::$kernel;
    }
}