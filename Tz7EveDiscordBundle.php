<?php


namespace Tz7\EveDiscordBundle;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Tz7\EveDiscordBundle\DependencyInjection\Compiler\BotServiceHandlerPass;


class Tz7EveDiscordBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new BotServiceHandlerPass());
    }
}
