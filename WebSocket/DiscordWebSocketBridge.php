<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\WebSocket;


use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\User\Game;
use Discord\WebSockets\Event;
use Discord\WebSockets\WebSocket;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tz7\EveDiscordBundle\Events\DiscordEvent;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface;
use Tz7\EveDiscordBundle\Service\BotServices;


class DiscordWebSocketBridge
{
    /**
     * @var BotServices
     */
    protected $botServices;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param BotServices              $botServices
     * @param EventDispatcherInterface $dispatcher
     * @param LoggerInterface          $logger
     */
    public function __construct(
        BotServices $botServices,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger
    ) {
        $this->botServices = $botServices;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    public function run(DiscordBotInterface $bot)
    {
        $discord = new Discord(["token" => $bot->getToken()]);
        $ws = new WebSocket($discord);
        $context = new DiscordWebSocketContext($this, $discord, $ws, $bot);

        $this->dispatcher->dispatch(DiscordEvent::PRE_CONNECT, new DiscordEvent($context));

        /** @noinspection PhpUnusedParameterInspection */
        $ws->on(
            'ready',
            function (Discord $discord, WebSocket $ws) use ($context)
            {
                $this->logger->info(sprintf('%s: "Discord WebSocket is ready!"', __CLASS__));
                $game = new Game(
                    [
                        'name' => 'EVE Online',
                        'url'  => null,
                        'type' => null
                    ],
                    true
                );
                $ws->updatePresence($game, false);

                $this->dispatcher->dispatch(DiscordEvent::POST_CONNECT, new DiscordEvent($context));

                $ws->loop->addTimer(
                    60 * 60 * 12,
                    function () use ($ws)
                    {
                        $ws->loop->stop();
                        sleep(30);
                    }
                );

                /** @var DiscordBotServiceInterface $service */
                foreach ($this->getActiveBotServices($context) as $service)
                {
                    $this->logger->info(
                        sprintf(
                            '%s: Registering "%s" tick every %ds',
                            __CLASS__,
                            $service->getHandlerType(),
                            $service->getTickPeriod()
                        )
                    );

                    $handler = $this->botServices->getServiceHandler($service->getHandlerType());
                    $serviceContext = $context->convertToServiceContext($service);
                    $ws->loop->addPeriodicTimer(
                        $service->getTickPeriod(),
                        function () use ($handler, $serviceContext)
                        {
                            $handler->tick($serviceContext);
                        }
                    );
                }

                $messageProcessorCallback = function (Message $message) use ($context)
                {
                    /** @var DiscordEvent $event */
                    $event = $this->dispatcher->dispatch(
                        DiscordEvent::ON_MESSAGE,
                        new DiscordEvent($context, ['message' => $message])
                    );

                    if ($event->isPropagationStopped())
                    {
                        return;
                    }

                    /** @var DiscordBotServiceInterface $triggered */
                    foreach ($this->getServicesTriggeredByMessage($context, $message) as $triggered)
                    {
                        // Calling triggered service handler
                        $this->botServices->handleMessageEvent(
                            $triggered->getHandlerType(),
                            new DiscordEvent(
                                $context,
                                [
                                    'message'     => $message,
                                    'bot_service' => $triggered
                                ]
                            )
                        );
                    }
                };

                $ws->on(Event::MESSAGE_CREATE, $messageProcessorCallback);
                $ws->on(Event::MESSAGE_UPDATE, $messageProcessorCallback);
            }
        );

        /** @noinspection PhpUnusedParameterInspection */
        $ws->on(
            'error',
            function ($error, $ws)
            {
                $this->logger->error(sprintf('%s: "%s"', __METHOD__, $error));
                throw new \Exception($error);
            }
        );

        $ws->on(
            'reconnecting',
            function ()
            {
                $this->logger->info(sprintf('%s: "Websocket is reconnecting.."', __METHOD__));
            }
        );

        $ws->on(
            'reconnected',
            function ()
            {
                $this->logger->info(sprintf('%s: "Websocket was reconnected.."', __METHOD__));
            }
        );

        $ws->run();
    }

    /**
     * @param DiscordWebSocketContext $context
     *
     * @return ArrayCollection
     */
    public function getActiveBotServices(DiscordWebSocketContext $context)
    {
        return $context
            ->getBot()
            ->getBotServices()
            ->filter(
                function (DiscordBotServiceInterface $service)
                {
                    return $service->isActive() && $this->botServices->getServiceHandler($service->getHandlerType());
                }
            );
    }

    /**
     * @param DiscordWebSocketContext $context
     * @param Message                 $message
     *
     * @return ArrayCollection
     */
    public function getServicesTriggeredByMessage(DiscordWebSocketContext $context, Message $message)
    {
        return $this
            ->getActiveBotServices($context)
            ->filter(
                function (DiscordBotServiceInterface $service) use ($context, $message)
                {
                    return $this->botServices
                        ->getServiceHandler($service->getHandlerType())
                        ->isTriggeredBy($context->convertToServiceContext($service), $message);
                }
            );
    }
}