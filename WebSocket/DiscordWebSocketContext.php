<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\WebSocket;


use Discord\Discord;
use Discord\Parts\Channel\Channel;
use Discord\WebSockets\WebSocket;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface;


class DiscordWebSocketContext
{
    /** @var DiscordWebSocketBridge; */
    protected $bridge;

    /** @var Discord */
    protected $discord;

    /** @var WebSocket */
    protected $webSocket;

    /** @var DiscordBotInterface */
    protected $bot;

    public function __construct(
        DiscordWebSocketBridge $bridge,
        Discord $discord,
        WebSocket $webSocket,
        DiscordBotInterface $bot
    ) {
        $this->bridge = $bridge;
        $this->discord = $discord;
        $this->webSocket = $webSocket;
        $this->bot = $bot;
    }

    /**
     * @param DiscordBotServiceInterface $service
     *
     * @return DiscordWebSocketServiceContext
     */
    public function convertToServiceContext(DiscordBotServiceInterface $service)
    {
        $newContext = new DiscordWebSocketServiceContext(
            $this->getBridge(),
            $this->getDiscord(),
            $this->getWebSocket(),
            $this->getBot(),
            $service
        );

        return $newContext;
    }

    /**
     * @return DiscordWebSocketBridge
     */
    public function getBridge()
    {
        return $this->bridge;
    }

    /**
     * @return Discord
     */
    public function getDiscord()
    {
        return $this->discord;
    }

    /**
     * @return WebSocket
     */
    public function getWebSocket()
    {
        return $this->webSocket;
    }

    /**
     * @return DiscordBotInterface
     */
    public function getBot()
    {
        return $this->bot;
    }
}