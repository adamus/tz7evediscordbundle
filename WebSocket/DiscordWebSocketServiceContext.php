<?php

/*
 * This file is part of the Tz7\EveDiscordBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveDiscordBundle\WebSocket;


use Discord\Discord;
use Discord\WebSockets\WebSocket;
use Tz7\EveDiscordBundle\Model\DiscordBotInterface;
use Tz7\EveDiscordBundle\Model\DiscordBotServiceInterface;


class DiscordWebSocketServiceContext extends DiscordWebSocketContext
{
    /** @var DiscordBotServiceInterface */
    protected $service;

    public function __construct(
        DiscordWebSocketBridge $bridge,
        Discord $discord,
        WebSocket $webSocket,
        DiscordBotInterface $bot,
        DiscordBotServiceInterface $service = null)
    {
        parent::__construct($bridge, $discord, $webSocket, $bot);
        $this->service = $service;
    }

    /**
     * @return DiscordBotServiceInterface
     */
    public function getService()
    {
        return $this->service;
    }
}